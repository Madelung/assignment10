package com.example.localadmin.moritz_advent;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.Button;
import java.util.ArrayList;

public class MainActivity extends WearableActivity {

    private static final int REQUEST_CODE = 1234;
    private Button speakButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setAmbientEnabled();

        speakButton = (Button) findViewById(R.id.speakButton);
    }

    public void speakButtonClicked(View v){
        startVoiceRecognitionActivity();
    }

    private void startVoiceRecognitionActivity(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Welchen Advent haben wir?");
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK)
        {
            ArrayList<String> matches =
                    data.getStringArrayListExtra(RecognizerIntent. EXTRA_RESULTS);
            String lValue = matches.get( 0 ).trim().toUpperCase();
            speakButton .setText(lValue);
            if (lValue.contains( "ERSTER ADVENT" ) ||
                    lValue.contains( "ERSTEN ADVENT" ) ||
                    lValue.contains( "ERSTE ADVENT" ))
                speakButton.setBackground(getResources().getDrawable(R.mipmap.advent01));
            else if (lValue.contains( "ZWEITER ADVENT" ) || lValue.contains( "ZWEITEN ADVENT" )|| lValue.contains( "ZWEITE ADVENT" ))
                speakButton.setBackground(getResources().getDrawable(R.mipmap.advent02));
            else if (lValue.contains( "DRITTER ADVENT" ) || lValue.contains( "DRITTEN ADVENT" )|| lValue.contains( "DRITTE ADVENT" ))
                speakButton.setBackground(getResources().getDrawable(R.mipmap.advent03));
            else if (lValue.contains( "VIERTER ADVENT" ) || lValue.contains( "VIERTEN ADVENT" )|| lValue.contains( "VIERTE ADVENT" ))
                speakButton.setBackground(getResources().getDrawable(R.mipmap.advent04));
            else
            speakButton .setBackgroundColor(Color. WHITE);
        }
        super .onActivityResult(requestCode, resultCode, data);
    }
}
